<?php
/*
	Session class
*/
class Session{

	// session initialization function
	public static function init(){
		if( version_compare( phpversion(), '5.4.0', '<' ) ){
			if( session_id() == '' ){
				session_start();
			}
		}else{
			if( session_status() == PHP_SESSION_NONE ){
				session_start();
			}
		}
	}

	// key and value set in session
	public static function set( $key, $val ){
		$_SESSION[$key] = $val;
	}

	// key return by session 
 	public static function get( $key ){
 		if( isset($_SESSION[$key]) ){
 			return $_SESSION[$key];
 		}else{
 			return false;
 		}
 	}

 	// unset key return by session 
	public static function ses_unset($key)
	{
		if (is_array($key))
		{
			foreach ($key as $k)
			{
				unset($_SESSION[$k]);
			}
			return;
		}
		unset($_SESSION[$key]);	
	}

	// if session check false function below
	public static function checkSession(){
		self::init();
		if( self::get( 'login_status' ) == FALSE ){
			self::destroy();
			header( "Location:../index.php" );
		}
	}

	// if session login true function below
	public static function checkLogin(){
		self::init();
		if( self::get( 'login_status' ) == TRUE ){
			header( "Location:hr/index.php" );
		}
	}

	// logout after session destroy function below
	public static function destroy(){
		session_destroy();
		header( "Location:../index.php" );
	}

	public static function customerDestroy(){
		session_destroy();
		header( "Location:login.php" );
	}

}