<?php
$path = realpath(dirname(__FILE__));
include_once($path.'/../lib/Database.php');
include_once ($path.'/../helpers/Format.php');
/*
* Student Info class
*/

class Controller{
	public $db;
	public $fm;
	public $msg = array();
	public function __construct()
    {
    	$this->db = new Database();
    	$this->fm = new Format();
    }
}