<?php
 // include config file here
$filepath = realpath(dirname(__FILE__));
 include($filepath.'/../config/Config.php');

/*
	Database class
*/
class Database{
	private $host   = DB_HOST;
	private $user   = DB_USER;
	private $pass   = DB_PASS;
	private $dbname = DB_NAME;

	public $link;
	public $error;

	public function __construct(){
		$this->connectDB();
	}

	// db connection function below
	private function connectDB(){
		$this->link = new mysqli( $this->host, $this->user, $this->pass, $this->dbname );
		if( ! $this->link ){
			$this->error = "Connection Faild".$this->link->connect_error;
			return false;
		}
	}

	// db select query function below
	public function select( $query ){
		$result = $this->link->query( $query ) or die( $this->link->error.__LINE__ );
		if( $result->num_rows > 0 ){
			return $result;
		}else{
			return false;
		}
	}
	// db insert query function below
	public function insert( $query ){
		$insert_row = $this->link->query( $query ) or die( $this->link->error>__LINE__ );
		if( $insert_row ){
			return $insert_row;
		}else{
			return false;
		}
	}

	// db update query function below
	public function update( $query ){
		$update_row = $this->link->query( $query ) or die( $this->link->error.__LINE__ );
		if( $update_row ){
			return $update_row;
		}else{
			return false;
		}
	}

	// db delete query function below
	public function delete( $query ){
		$delete_row = $this->link->query( $query ) or die( $this->link->error.__LINE__ );
		if( $delete_row ){
			return $delete_row;
		}else{
			return false;
		}

	}
}