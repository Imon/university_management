<?php 
$path = realpath(dirname(__FILE__));
include_once($path.'/lib/Session.php');
Session::checkLogin();
	spl_autoload_register(function($class){
	    include_once 'classes/'.$class.'.php';
	});
	$adl = new Administration_login();
	$msg =array();
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['login'])){
		$msg = $adl->check_authentication($_POST);
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login Authentication</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="assets/css/login.css">
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
 
</head>
<body>
	<div id='login'>
		<!--<h1>World University of<span class='orangestop'>Bangladesh</span></h1>-->
		<h1 class="sitelogo"><img src="assets/images/wub_logo.png" alt="" /></a></h1>
		<?php 
			if(isset($msg['error'])){
				printf('<span class="error">%s</span>', $msg['error']);
			}
		?>
		<form action="" method="post">
		  <span class='input'>
			<span class='icon username-icon fontawesome-user'></span>
			<input type='text' name="email" class='username' placeholder='Enter your email address'>
		 </span>
		  <span class='input'>
			<span class='password-icon-style icon password-icon fontawesome-lock'></span>
			<input type='password' name="password" class='password' placeholder='Password'>
		  </span>
		  <div class='divider'></div>
		  <button type="submit" name="login">LOG IN</button>
		</form>
		<div class='forgot'><a href="#">Forgot Password?</a></div>
		<div class='divider'></div>
		<p class="backtosite"><a href="">Back to <span class='reg'>WUB</span></a></p>
	</div>
<script src="assets/js/index.js"></script>
</body>
</html>