<?php include 'inc/header.php'; ?>
<?php include 'inc/sidebar.php'; ?>

<?php 
$msg = array();
  if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['st_btn'])){
    $admin_id = Session::get('userID');
    $msg = $st->save_student_fee_category($_POST, $admin_id);

  }elseif($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_btn'])){
    $admin_id = Session::get('userID');
    $msg = $st->update_student_fee_category($_POST, $admin_id);

  }elseif($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['fee_btn'])){
    $msg = $st->update_status_fee_category($_POST);

  }

  if(isset($_GET['del_cat_id']) && $_GET['del_cat_id'] !=NULL){
    $msg = $st->delete_student_fee_category($_GET['del_cat_id']);
  }
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header padding-bottom">
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-xs-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Add Fee Category</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                  <?php if(isset($msg['error'])){
                      printf('<span class="flash_error">%s</span>', $msg['error']); 
                    }elseif(isset($msg['success'])){
                      printf('<span class="flash_success">%s</span>', $msg['success']); 
                    }
                  ?>
                      <div class="row">
                        <div class="form-group col-sm-4">
                        <?php if(isset($_GET['cat_id']) && $_GET['cat_id'] !=NULL){ 
                            $fee_row = $st->get_fee_category_by_id($_GET['cat_id']);
                            while($result = $fee_row->fetch_assoc()){
                          ?>
                         <form method="post" action="<?= htmlspecialchars($_SERVER['PHP_SELF']) ?>">
                          <label for="fee_name">Fee Category Name <span class="required">*</span></label>
                          <input class="form-control" name="fee_name" id="fee_name" type="text" value="<?= $result['fee_name']; ?>">
                          <input class="form-control" name="fee_id" type="hidden" value="<?= $result['fee_id']; ?>">
                          <?php if(isset($msg['fee_name'])){printf('<span class="error">%s</span>', $msg['fee_name']); }?>
                          <div class="row button">
                            <div class="col-xs-12">
                              <div class="button_sep text-left">
                                 <input class="btn btn-info" id="submitbutton" type="submit" name="update_btn" value="Update">
                              </div>
                            </div>
                          </div>
                        </form>
                        <?php } ?>
                        <?php }else{?>
                         <form method="post" action="<?= htmlspecialchars($_SERVER['PHP_SELF']) ?>" enctype="multipart/form-data">
                          <label for="fee_name">Fee Category Name <span class="required">*</span></label>
                          <input class="form-control" name="fee_name" id="fee_name" type="text">
                          <?php if(isset($msg['fee_name'])){printf('<span class="error">%s</span>', $msg['fee_name']); }?>
                          <div class="row button">
                            <div class="col-xs-12">
                              <div class="button_sep text-left">
                                 <input class="btn btn-info" id="submitbutton" type="submit" name="st_btn" value="Submit">
                              </div>
                            </div>
                          </div>
                        </form>
                        <?php } ?>
                        </div>
                        <div class="form-group col-sm-8">
                          <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Serial No.</th>
                                    <th>Fee Category Name</th>
                                    <th>Admin Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                              $get_fee = $st->get_student_fee_category();
                              if($get_fee){
                                $i=0;
                                while($row = $get_fee->fetch_assoc()){
                                  $i++;
                            ?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $row['fee_name']; ?></td>
                                    <td><?= $row['fullname']; ?></td>
                                    <td>
                                      <?php if($row['fee_status'] == '1'){?>
                                      <form action="" method="post">
                                        <input type="hidden" name="fee_status" value="0">
                                        <input type="hidden" name="fee_id" value="<?= $row['fee_id']; ?>">
                                        <button type="submit" name="fee_btn">Deactived</button>
                                      </form>
                                      <?php }else{?>
                                      <form action="" method="post">
                                        <input type="hidden" name="fee_status" value="1">
                                        <input type="hidden" name="fee_id" value="<?= $row['fee_id']; ?>">
                                        <button type="submit" name="fee_btn">Active</button>
                                      </form>
                                      <?php } ?>
                                    </td>
                                    <td><a href="?cat_id=<?= $row['fee_id']; ?>">Edit</a> || <a onclick="return confirm('Are you sure to delete this item!!');" href="?del_cat_id=<?= $row['fee_id']; ?>">Delete</a></td>
                                    
                                </tr>
                                <?php } ?>
                              <?php } ?>
                          </tbody>
                          </table>
                        </div>
                      </div>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- ./box-body -->
              <div class="box-footer">
                <div class="row">
                  <div class="col-xs-12">
                  </div>
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
         </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include 'inc/footer.php'; ?>
