<?php include 'inc/header.php'; ?>
<?php include 'inc/sidebar.php'; ?>

<?php 
$msg = array();
  if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['st_btn'])){
	  
    $admin_id = Session::get('userID');
    $msg = $st->save_student_money_receipt($_POST, $admin_id);

  }
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header padding-bottom">
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-xs-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Money Receipt</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                  <?php if(isset($msg['error'])){
                      printf('<span class="flash_error">%s</span>', $msg['error']); 
                    }elseif(isset($msg['success'])){
                      printf('<span class="flash_success">%s</span>', $msg['success']); 
                    }
                  ?>
					         <form action="" method="post">
                      <div class="row">
                      <div class="col-xs-12">
                        <div class="row">
                          <div class="form-group col-sm-4">
                          <label for="department">Program <span class="required">*</span></label>
                          <select id="department" name="department" class="form-control select2" style="width: 100%;">
                            <option value="">Select Program</option>
                            <option>B. Pharm</option>
                            <option>BA in English</option>
                            <option>Bachelor in THM</option>
                            <option>BBA</option>
                            <option>BSc in Architecture</option>
                            <option>BSc in Civil Engineering</option>
                            <option>BSc in CSE</option>
                            <option>BSc in EEE</option>
                            <option>BSc in MTE</option>
                            <option>BSc in Textile Engineering</option>
                            <option>LLB (2 Years)</option>
                            <option>LLB (4 Years)</option>
                            <option>LLM (1 Year)</option>
                            <option>LLM (2 Years)</option>
                            <option>MA in English</option>
                            <option>MBA</option>
                          </select>                          
                          </div>
                          <div class="form-group col-sm-4">
                          <label for="batch">Batch <span class="required">*</span></label>
                          <select id="batch" name="batch" class="form-control select2" style="width: 100%;">
                            
                          </select>                          
                          </div>
                          <div class="form-group col-sm-4">
                          <label for="registration_id">RegistrationID <span class="required">*</span></label>
                          <select id="registration_id" name="registration_id" class="form-control select2" style="width: 100%;">
                            
                          </select>                          
                          </div>
                        </div>
                        <hr/>
                        <div class="row">
                          <div class="form-group col-sm-8">
                          <label for="student_name">Name <span class="required">*</span></label>
                          <div id="student_name">
                            <span class="dispaly_name"></span>
                          </div>                   
                          </div>
                          <div class="form-group col-sm-4">
                          <label for="receipt_date">Receipt Date <span class="required">*</span></label>
                          <input class="form-control" name="receipt_date" id="receipt_date" type="text" value="<?= date('d-m-Y'); ?>">                    
                          </div>
                        </div>
                      </div>
                        <div class="col-sm-12">
                          <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="text-center" width="10%">Serial No.</th>
                                    <th class="text-center" width="55%">Description</th>
                                    <th class="text-center" width="35%">Amount in Tk.</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                              $get_fee_cat = $st->get_money_receipt_fee_category();
                              if($get_fee_cat){
                                $i = 0;
                                while($row = $get_fee_cat->fetch_assoc()){
                                  $i++;
                            ?>
                              <tr>
                                <td><?= $i; ?></td>
                                <td><?= $row['fee_name']; ?></td>
                                <td>
								<input min="1" class="form-control cal" type="number" name="fee_amount[<?= $row['fee_id']; ?>]">
								</td>
                              </tr>
                              <?php } ?>
                              <?php } ?>
                              <tr>
                                <td colspan="3">
                                  <hr/>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="1">
                                  
                                </td>
                                <td>
                                  <div class="text-left">
                                  
                                     <input class="btn btn-info" id="submitbutton" type="submit" name="st_btn" value="Submit">
                                  </div>
                                </td>
                                <td>
                                <label for="ssc_result">TK. </label> 
                                <span id="sum"> 0.00</span>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
					  </form>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>

              <script type="text/javascript">

              </script>
              <!-- ./box-body -->
              <div class="box-footer">
                <div class="row">
                  <div class="col-xs-12">
                  </div>
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
         </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include 'inc/footer.php'; ?>
