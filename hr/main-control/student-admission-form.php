<?php include 'inc/header.php'; ?>
<?php include 'inc/sidebar.php'; ?>

<?php 
$msg = array();
  if($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['st_btn']){
    $admin_id = Session::get('userID');
    $msg = $st->save_student_info($_POST, $_FILES, $admin_id);

  }

?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header padding-bottom">
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-xs-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Student Admission</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                  <?php if(isset($msg['error'])){
                      printf('<span class="flash_error">%s</span>', $msg['error']); 
                    }elseif(isset($msg['success'])){
                      printf('<span class="flash_success">%s</span>', $msg['success']); 
                    }
                  ?>
                    <form method="post" action="<?= htmlspecialchars($_SERVER['PHP_SELF']) ?>" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-xs-12">
                          <p class="text-left">
                            <strong>OFFICIAL DETAILS:-</strong>
                          </p>
                        <hr>
                        </div>
                        <div class="form-group col-sm-4">
                          <label for="session">Session <span class="required">*</span></label>
                          <select id="session" name="session" class="form-control select2" style="width: 100%;">
                            <option value="">Select Session</option>
                            <?php 
                              $date = date('Y');
                              $month = strtotime ( '+2 year' , strtotime ( $date ) ) ;
                               $month_current = date ( 'Y' , $month );

                                $startdate=strtotime($month_current);
                               $enddate=strtotime("-10 year",$startdate);



                              while ($startdate>$enddate) {
                                 $year_month=date("Y", $startdate);
                                 $year_update=date("Y", $startdate);
                                 $year_value=$year_update+1;
                                 ?>
                                 
                                 <option><?php echo $year_month; echo "-"; echo $year_value; ?></option>
                                 <?php
                                
                               $startdate = strtotime("-1 year", $startdate);

                              }
                            ?>
                          </select>
                          <?php if(isset($msg['session'])){printf('<span class="error">%s</span>', $msg['session']); }?>
                        </div>
                        <div class="form-group col-sm-4">
                          <label for="student_id">Registration ID <span class="required">*</span></label>
                          <input class="form-control" name="registration_id" id="student_id" type="text">
                          <?php if(isset($msg['registration_id'])){printf('<span class="error">%s</span>', $msg['registration_id']); }?>
                        </div>
                        <div class="form-group col-sm-4">
                          <label for="roll_no">Student Roll <span class="required">*</span></label>
                          <input class="form-control" name="roll_no" id="roll_no" type="text">
                          <?php if(isset($msg['roll_no'])){printf('<span class="error">%s</span>', $msg['roll_no']); }?>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-sm-4">
                          <label for="department">Program <span class="required">*</span></label>
                          <select id="department" name="department" class="form-control select2" style="width: 100%;">
                            <option value="">Select Program</option>
                            <option>B. Pharm</option>
                            <option>BA in English</option>
                            <option>Bachelor in THM</option>
                            <option>BBA</option>
                            <option>BSc in Architecture</option>
                            <option>BSc in Civil Engineering</option>
                            <option>BSc in CSE</option>
                            <option>BSc in EEE</option>
                            <option>BSc in MTE</option>
                            <option>BSc in Textile Engineering</option>
                            <option>LLB (2 Years)</option>
                            <option>LLB (4 Years)</option>
                            <option>LLM (1 Year)</option>
                            <option>LLM (2 Years)</option>
                            <option>MA in English</option>
                            <option>MBA</option>
                          </select>
                          <?php if(isset($msg['department'])){printf('<span class="error">%s</span>', $msg['department']); }?>
                        </div>
                        <div class="form-group col-sm-4">
                          <label for="batch">Batch <span class="required">*</span></label>
                          <select id="batch" name="batch" class="form-control select2" style="width: 100%;">
                            <option value="">Select Batch</option>
                            <option value="31">31 (B)</option>
                          </select>
                          <?php if(isset($msg['batch'])){printf('<span class="error">%s</span>', $msg['batch']); }?>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12">
                          <p class="text-left">
                            <strong>Education Qualification:-</strong>
                          </p>
                        <hr>
                        </div>
                        <div class="col-xs-12">
                          <p class="text-left">
                            <strong>SSC:-</strong>
                          </p>
                        </div>
                        <div class="form-group col-sm-2">
                          <label for="ssc_passing_year">Passing Year <span class="required">*</span></label>
                          <select id="ssc_passing_year" name="ssc_passing_year" class="form-control select2" style="width: 100%;">
                            <option value="">Select Passing Year</option>
                            <?php 
                               $month_current = date ( 'Y');

                                $startdate=strtotime($month_current);
                               $enddate=strtotime("-5 year",$startdate);



                              while ($startdate>$enddate) {
                                 $year_month=date("Y", $startdate);
                                 ?>
                                 
                                 <option><?php echo $year_month;?></option>
                                 <?php
                                
                               $startdate = strtotime("-1 year", $startdate);

                              }
                            ?>
                          </select>
                          <?php if(isset($msg['ssc_passing_year'])){printf('<span class="error">%s</span>', $msg['ssc_passing_year']); }?>
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="ssc_group">Group <span class="required">*</span></label>
                          <input class="form-control" name="ssc_group" id="ssc_group" type="text">
                          <?php if(isset($msg['ssc_group'])){printf('<span class="error">%s</span>', $msg['ssc_group']); }?>
                        </div>
                        <div class="form-group col-sm-2">
                          <label for="ssc_roll">Roll No. <span class="required">*</span></label>
                          <input class="form-control" name="ssc_roll" id="ssc_roll" type="text">
                          <?php if(isset($msg['ssc_roll'])){printf('<span class="error">%s</span>', $msg['ssc_roll']); }?>
                        </div>
                        <div class="form-group col-sm-2">
                          <label for="ssc_result">GPA<span class="required">*</span></label>
                          <input class="form-control" name="ssc_result" id="ssc_result" type="text">
                          <?php if(isset($msg['ssc_result'])){printf('<span class="error">%s</span>', $msg['ssc_result']); }?>
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="ssc_board">Board Name <span class="required">*</span></label>
                          <input class="form-control" name="ssc_board" id="ssc_board" type="text">
                          <?php if(isset($msg['ssc_board'])){printf('<span class="error">%s</span>', $msg['ssc_board']); }?>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12">
                          <p class="text-left">
                            <strong>HSC:-</strong>
                          </p>
                        </div>
                        <div class="form-group col-sm-2">
                          <label for="hsc_passing_year">Passing Year <span class="required">*</span></label>
                          <select id="hsc_passing_year" name="hsc_passing_year" class="form-control select2" style="width: 100%;">
                            <option value="">Select Passing Year</option>
                            <?php 
                              $month_current = date ( 'Y');

                              $startdate=strtotime($month_current);
                              $enddate=strtotime("-5 year",$startdate);
                              while ($startdate>$enddate) {
                                 $year_month=date("Y", $startdate);
                                 ?>
                                 
                                 <option><?php echo $year_month;?></option>
                                 <?php
                                
                               $startdate = strtotime("-1 year", $startdate);

                              }
                            ?>
                          </select>
                          <?php if(isset($msg['hsc_passing_year'])){printf('<span class="error">%s</span>', $msg['hsc_passing_year']); }?>
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="hsc_group">Group <span class="required">*</span></label>
                          <input class="form-control" name="hsc_group" id="hsc_group" type="text">
                          <?php if(isset($msg['hsc_group'])){printf('<span class="error">%s</span>', $msg['hsc_group']); }?>
                        </div>
                        <div class="form-group col-sm-2">
                          <label for="hsc_roll">Roll No. <span class="required">*</span></label>
                          <input class="form-control" name="hsc_roll" id="hsc_roll" type="text">
                          <?php if(isset($msg['hsc_roll'])){printf('<span class="error">%s</span>', $msg['hsc_roll']); }?>
                        </div>
                        <div class="form-group col-sm-2">
                          <label for="hsc_result">GPA<span class="required">*</span></label>
                          <input class="form-control" name="hsc_result" id="hsc_result" type="text">
                          <?php if(isset($msg['hsc_result'])){printf('<span class="error">%s</span>', $msg['hsc_result']); }?>
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="hsc_board">Board Name<span class="required">*</span></label>
                          <input class="form-control" name="hsc_board" id="hsc_board" type="text">
                          <?php if(isset($msg['hsc_board'])){printf('<span class="error">%s</span>', $msg['hsc_board']); }?>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12">
                          <p class="text-left">
                            <strong>PERSONAL DETAILS:-</strong>
                          </p>
                        <hr>
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="student_name">Full Name <span class="required">*</span></label>
                          <input class="form-control" name="student_name" id="student_name" type="text">
                          <?php if(isset($msg['student_name'])){printf('<span class="error">%s</span>', $msg['student_name']); }?>
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="father_name">Father's Name. <span class="required">*</span></label>
                          <input class="form-control" name="father_name" id="father_name" type="text">
                          <?php if(isset($msg['father_name'])){printf('<span class="error">%s</span>', $msg['father_name']); }?>
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="mother_name">Mother's Name. <span class="required">*</span></label>
                          <input class="form-control" name="mother_name" id="mother_name" type="text">
                          <?php if(isset($msg['mother_name'])){printf('<span class="error">%s</span>', $msg['mother_name']); }?>
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="birth_date">Birthday <span class="required">*</span></label>
                          <div class="input-group date">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input class="form-control" name="birth_date" id="birth_date" type="text">
                            <?php if(isset($msg['birth_date'])){printf('<span class="error">%s</span>', $msg['birth_date']); }?>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-sm-3">
                          <label for="religion">Religion <span class="required">*</span></label>
                          <select id="religion" name="religion" class="form-control select2" style="width: 100%;">
                            <option value="">Select Religion</option>
                            <option>Muslim</option>
                            <option>Hindu</option>
                          </select> 
                          <?php if(isset($msg['religion'])){printf('<span class="error">%s</span>', $msg['religion']); }?>                       
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="gender">Gender <span class="required">*</span></label>
                          <select id="gender" name="gender" class="form-control select2" style="width: 100%;">
                            <option value="">Select Gender</option>
                            <option>Male</option>
                            <option>Female</option>
                            <option>Other</option>
                          </select>
                          <?php if(isset($msg['gender'])){printf('<span class="error">%s</span>', $msg['gender']); }?>
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="blood_group">Blood Group</label>
                          <select id="blood_group" name="blood_group" class="form-control select2" style="width: 100%;">
                            <option value="">Select Blood Group</option>
                            <option>A+</option>
                            <option>A-</option>
                            <option>B+</option>
                            <option>B-</option>
                            <option>O+</option>
                            <option>O-</option>
                            <option>AB+</option>
                            <option>AB-</option>
                          </select>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12">
                          <p class="text-left">
                            <strong>Contact DETAILS:-</strong>
                          </p>
                        <hr>
                        </div>
                        <div class="form-group col-sm-6">
                          <label for="permanent_address">Permanent Address <span class="required">*</span></label>
                          <textarea class="form-control" name="permanent_address" id="permanent_address"></textarea>
                          <?php if(isset($msg['permanent_address'])){printf('<span class="error">%s</span>', $msg['permanent_address']); }?>
                        </div>
                        <div class="form-group col-sm-6">
                          <label for="present_address">Present Address<span class="required">*</span></label>
                          <textarea class="form-control" name="present_address" id="present_address"></textarea>
                          <?php if(isset($msg['present_address'])){printf('<span class="error">%s</span>', $msg['present_address']); }?>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-sm-4">
                          <label for="email">Email <span>(Optional)</span></label>
                          <input class="form-control" name="email" id="email" type="text">
                        </div>
                        <div class="form-group col-sm-4">
                          <label for="mobile_no">Mobile No. <span class="required">*</span></label>
                          <input class="form-control" name="mobile_no" id="mobile_no" type="text">
                          <?php if(isset($msg['mobile_no'])){printf('<span class="error">%s</span>', $msg['mobile_no']); }?>
                        </div>
                        <div class="form-group col-sm-4">
                          <label for="image">Upload Photo <span class="required">*</span></label>
                          <input name="image" id="image" type="file">
                        </div>
                      </div>
                      <div class="row button">
                        <div class="col-xs-12">
                          <div class="button_sep text-left">
                             <input class="btn btn-info" id="submitbutton" type="submit" name="st_btn" value="Submit">
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- ./box-body -->
              <div class="box-footer">
                <div class="row">
                  <div class="col-xs-12">
                  </div>
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
         </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include 'inc/footer.php'; ?>
