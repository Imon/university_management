
<?php include 'inc/header.php'; ?>
<?php include 'inc/sidebar.php'; ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header padding-bottom">
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-xs-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Teacher Information</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
             <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <form>
                      <div class="row">
                        <div class="col-xs-12">
                          <p class="text-left">
                            <strong>OFFICIAL DETAILS:-</strong>
                          </p>
                        <hr>
                        </div>

                        <div class="form-group col-sm-3">
                          <label for="department">Department <span class="required">*</span></label>
                          <select id="department" name="department" class="form-control select2" style="width: 100%;">
                            <option value="">Select Department</option>
                            <option value="2">Pharm</option>
                            <option value="7">English</option>
                            <option value="14">THM</option>
                            <option value="9">Business</option>
                            <option value="11">Architecture</option>
                            <option value="10">Civil Engineering</option>
                            <option value="4">CSE</option>
                            <option value="5">EEE</option>
                            <option value="1">MTE</option>
                            <option value="6">Textile Engineering</option>
                            <option value="15">Law</option>
                          </select>                        
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="designation">Designation <span class="required">*</span></label>
                          <select id="designation" name="designation" class="form-control select2" style="width: 100%;">
                            <option value="">Select Designation</option>
                            <option>Lecturer</option>
                            <option>Lab Assistant</option>
                          </select>
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="joining_date">Joining Date</label>
                          <div class="input-group date">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input class="form-control" name="joining_date" id="joining_date" type="text">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12">
                          <p class="text-left">
                            <strong>PERSONAL DETAILS:-</strong>
                          </p>
                        <hr>
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="student_name">Full Name <span class="required">*</span></label>
                          <input class="form-control" name="student_name" id="student_name" type="text">
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="father_name">Father's Name. <span class="required">*</span></label>
                          <input class="form-control" name="father_name" id="father_name" type="text">
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="mother_name">Mother's Name. <span class="required">*</span></label>
                          <input class="form-control" name="mother_name" id="mother_name" type="text">
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="latest_education">Latest Education</label>
                          <input class="form-control" name="latest_education" id="latest_education" type="text">
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-sm-3">
                          <label for="birth_date">Birthday <span class="required">*</span></label>
                          <div class="input-group date">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input class="form-control" name="birth_date" id="birth_date" type="text">
                          </div>
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="religion">Religion <span class="required">*</span></label>
                          <select id="religion" name="religion" class="form-control select2" style="width: 100%;">
                            <option value="">Select Religion</option>
                            <option>Muslim</option>
                            <option>Hindu</option>
                          </select>                        
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="gender">Gender <span class="required">*</span></label>
                          <select id="gender" name="gender" class="form-control select2" style="width: 100%;">
                            <option value="">Select Gender</option>
                            <option>Male</option>
                            <option>Female</option>
                            <option>Other</option>
                          </select>
                        </div>
                        <div class="input_fields_wrap">
                          <div class="form-group col-sm-3">
                            <label for="publication_1">Publication</label>
                            <input class="form-control custom-field" name="publication_1" id="publication_1" type="text">
                            <a href="#" class="add_field_button">+</a>
                          </div>
                        </div> 
                      </div>
                      <div class="row">
                        <div class="col-xs-12">
                          <p class="text-left">
                            <strong>Contact DETAILS:-</strong>
                          </p>
                        <hr>
                        </div>
                        <div class="form-group col-sm-6">
                          <label for="permanent_address">Permanent Address <span class="required">*</span></label>
                          <textarea class="form-control" name="permanent_address" id="permanent_address"></textarea>
                        </div>
                        <div class="form-group col-sm-6">
                          <label for="present_address">Present Address<span class="required">*</span></label>
                          <textarea class="form-control" name="present_address" id="present_address"></textarea>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-sm-3">
                          <label for="email">Email <span class="required">*</span></label>
                          <input class="form-control" name="email" id="email" type="text">
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="mobile_no">Mobile No. <span class="required">*</span></label>
                          <input class="form-control" name="mobile_no" id="mobile_no" type="text">
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="telephone_no">Telephone No. <span>(Optional)</span></label>
                          <input class="form-control" name="telephone_no" id="telephone_no" type="text">
                        </div>
                        <div class="form-group col-sm-3">
                          <label for="fax">fax <span>(Optional)</span></label>
                          <input class="form-control" name="fax" id="fax" type="text">
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-sm-4">
                          <label for="image">Upload Photo <span>(Optional)</span></label>
                          <input name="image" id="image" type="file">
                        </div>
                      </div>
                      <div class="row button">
                        <div class="col-xs-12">
                          <div class="button_sep text-left">
                             <input class="btn btn-info" id="submitbutton" type="submit" name="yt0" value="Submit">
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- ./box-body -->
              <div class="box-footer">
                <div class="row">
                  <div class="col-xs-12">
                  </div>
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
         </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include 'inc/footer.php'; ?>
