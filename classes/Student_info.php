<?php
$path = realpath(dirname(__FILE__));
include_once($path.'/../lib/Controller.php');
/*
* Student Info class
*/

class Student_info extends Controller {
	
	public function __construct()
    {
        parent::__construct();
    }

    public function save_student_info($post, $file, $admin_id){
	$registration_id = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['registration_id']));
	$roll_no = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['roll_no']));
	$student_name = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['student_name']));
	$father_name = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['father_name']));
	$mother_name = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['mother_name']));
	$birth_date = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($this->fm->sql_date_format($post['birth_date'])));
	$mobile_no = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['mobile_no']));
	$email = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['email']));
	$gender = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['gender']));
	$permanent_address = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['permanent_address']));
	$present_address = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['present_address']));
	$religion = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['religion']));
	$blood_group = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['blood_group']));
	$ssc_passing_year = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['ssc_passing_year']));
	$ssc_group = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['ssc_group']));
	$ssc_roll = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['ssc_roll']));
	$ssc_result = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['ssc_result']));
	$ssc_board = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['ssc_board']));
	$hsc_passing_year = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['hsc_passing_year']));
	$hsc_group = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['hsc_group']));
	$hsc_roll = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['hsc_roll']));
	$hsc_result = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['hsc_result']));
	$hsc_board = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['hsc_board']));

	$department = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['department']));
	$session = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['session']));
	$batch = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['batch']));
	$image = $this->profile_image_created($file); //mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['image']));
	$user_admin = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($admin_id));
	
	
		$error = '';
	    if(empty($registration_id)) {
	        $error = true;
	        $this->msg['registration_id'] = "Registration ID is required!";
	    }else if(!preg_match("/^[a-zA-Z0-9\s\/]+$/",$registration_id)){
	    	$error = true;
	        $this->msg['registration_id'] = "registration ID must alphabets, slash and space";
	    }
	    if(empty($roll_no)) {
	        $error = true;
	        $this->msg['roll_no'] = "Roll NO. is required!";
	    }else if(!preg_match("/^[0-9]+$/",$roll_no)){
	    	$error = true;
	        $this->msg['roll_no'] = "Roll NO. must be only number";
	    }
	    if(empty($student_name)) {
	        $error = true;
	        $this->msg['student_name'] = "Student Name is required!";
	    }else if(!preg_match("/^[a-zA-Z\s]+$/",$student_name)){
	    	$error = true;
	        $this->msg['student_name'] = "Student Name must only alphabets and space";
	    }
	    if(empty($father_name)) {
	        $error = true;
	        $this->msg['father_name'] = "Father Name is required!";
	    }else if(!preg_match("/^[a-zA-Z\s]+$/",$father_name)){
	    	$error = true;
	        $this->msg['father_name'] = "Father Name must only alphabets and space";
	    }
	    if(empty($mother_name)) {
	        $error = true;
	        $this->msg['mother_name'] = "Mother Name is required!";
	    }else if(!preg_match("/^[a-zA-Z\s]+$/",$mother_name)){
	    	$error = true;
	        $this->msg['mother_name'] = "Mother Name must only alphabets and space";
	    }
	    if(empty($birth_date)) {
	        $error = true;
	        $this->msg['birth_date'] = "Birth of Date is required!";
	    }
	    if(empty($mobile_no)) {
	        $error = true;
	        $this->msg['mobile_no'] = "Mobile No. is required!";
	    }else if(!preg_match("/^[0-9]+$/",$mobile_no)){
	    	$error = true;
	        $this->msg['mobile_no'] = "Mobile NO. must be only number";
	    }

	    if(empty($gender)) {
	        $error = true;
	        $this->msg['gender'] = "Gender is required!";
	    }

	    if(empty($permanent_address)) {
	        $error = true;
	        $this->msg['permanent_address'] = "Permanent address is required!";
	    }
	    if(empty($present_address)) {
	        $error = true;
	        $this->msg['present_address'] = "Present address is required!";
	    }
	    if(empty($religion)) {
	        $error = true;
	        $this->msg['religion'] = "Religion is required!";
	    } 
	    if(empty($ssc_passing_year)) {
	        $error = true;
	        $this->msg['ssc_passing_year'] = "Passing year is required!";
	    } 
	    if(empty($ssc_group)) {
	        $error = true;
	        $this->msg['ssc_group'] = "SSC group is required!";
	    } 
	    if(empty($ssc_roll)) {
	        $error = true;
	        $this->msg['ssc_roll'] = "SSC roll is required!";
	    } 
	    if(empty($ssc_result)) {
	        $error = true;
	        $this->msg['ssc_result'] = "SSC result is required!";
	    } 
	    if(empty($ssc_board)) {
	        $error = true;
	        $this->msg['ssc_board'] = "SSC board is required!";
	    }
		if(empty($hsc_passing_year)) {
	        $error = true;
	        $this->msg['hsc_passing_year'] = "HSC passing year is required!";
	    } 
	    if(empty($hsc_group)) {
	        $error = true;
	        $this->msg['hsc_group'] = "HSC group is required!";
	    } 
	    if(empty($hsc_roll)) {
	        $error = true;
	        $this->msg['hsc_roll'] = "HSC roll is required!";
	    } 
	    if(empty($hsc_result)) {
	        $error = true;
	        $this->msg['hsc_result'] = "HSC result is required!";
	    } 
	    if(empty($hsc_board)) {
	        $error = true;
	        $this->msg['hsc_board'] = "HSC board is required!";
	    }

	   	if(empty($department)) {
	        $error = true;
	        $this->msg['department'] = "Program is required!";
	    }

	    if(empty($session)) {
	        $error = true;
	        $this->msg['session'] = "Session is required!";
	    }
	    if(empty($batch)) {
	        $error = true;
	        $this->msg['batch'] = "Batch is required!";
	    }
	    if($image != true){
			$error = true;
	    }
	    if (!$error) {
			$upload_file = "../../uploads/".$image[1];
			move_uploaded_file($image[0], $upload_file);
    		$query = "INSERT INTO student_info (registration_id, roll_no, student_name, father_name, mother_name, birth_date, mobile_no,
    							email, gender, permanent_address, present_address, religion, blood_group, ssc_roll, ssc_result, ssc_passing_year, 
    							ssc_board, ssc_group, hsc_roll, hsc_result, hsc_passing_year, hsc_board, hsc_group, department, session, 
    							batch, image, user_admin) 
    				  VALUES ('$registration_id', '$roll_no', '$student_name', '$father_name', '$mother_name', '$birth_date', '$mobile_no', 
    				  		'$email', '$gender', '$permanent_address', '$present_address', '$religion', '$blood_group', '$ssc_roll',
    				  		'$ssc_result', '$ssc_passing_year', '$ssc_board', '$ssc_group', '$hsc_roll', '$hsc_result', '$hsc_passing_year',
    				  		'$hsc_board', '$hsc_group', '$department', '$session', '$batch', '$upload_file', '$user_admin')";
		    $inseruser = $this->db->insert($query);
		    if($inseruser){
		    	$this->msg['success'] = "Admitted Successfully!";
		    }else{
		    	$this->msg['error'] = "Error in Admission...Please try again later!!";
		    }
	    }
	     return $this->msg;
    }

    private function profile_image_created($_file){
    	$permited_type = array('jpg', 'jpeg', 'png', 'gif');
    	$file_name = $_file['image']['name'];
    	$file_size = $_file['image']['size'];
    	$file_temp = $_file['image']['tmp_name'];

    	$file_divided = explode('.', $file_name);
    	$file_ex = strtolower(end($file_divided));
    	$unique_file = substr(md5(time()), 0, 20).".$file_ex";

		list($width, $height) = @getimagesize($file_temp);

		if(empty($file_name)){
			$this->msg['error'] = "Image required !!";
			return $this->msg;
		}elseif($width > "150" || $height > "150") {
		    $this->msg['error'] = "Error : image size must be 150 x 150 pixels.";
		    return $this->msg;
		}elseif(in_array($file_ex, $permited_type) === FALSE){
			$this->msg['error'] = "You can upload only :-". implode(', ', $permited_type);
			return $this->msg;
		}else{
			return array($file_temp, $unique_file);
		}
    }



    // this function for Add fee list
    public function save_student_fee_category($post, $admin_id){
		$fee_name = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['fee_name']));
		$admin_id = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($admin_id));
		$error = '';
		if(empty($fee_name)) {
	        $error = true;
	        $this->msg['fee_name'] = "Batch is required!";
	    }else if(!preg_match("/^[a-zA-Z\s]+$/",$fee_name)){
	    	$error = true;
	        $this->msg['fee_name'] = "Category Name must only alphabets and space";
	    }

	    if (!$error) {
    		$query = "INSERT INTO tbl_fee_category (fee_name, admin_id) 
    				  VALUES('$fee_name','$admin_id')";
		    $insercat = $this->db->insert($query);
		    if($insercat){
		    	$this->msg['success'] = "Save Successfully!";
		    }else{
		    	$this->msg['error'] = "Error in Save...Please try again later!!";
		    }
	    }
	     return $this->msg;
    }

    // this function get student fee category

    public function get_student_fee_category(){
 		$query = "SELECT tbl_fee_category.*, admin_info.fullname
 				   FROM tbl_fee_category
 				   INNER JOIN admin_info ON tbl_fee_category.admin_id = admin_info.id";
	    return $result = $this->db->select($query);
    }

    public function get_fee_category_by_id($cat_id){
		$query = "SELECT *FROM tbl_fee_category WHERE fee_id = '$cat_id'";
	    return $result = $this->db->select($query);
    }
    public function update_student_fee_category($post, $admin_id){
		$fee_name = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['fee_name']));
		$admin_id = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($admin_id));
		$fee_id = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['fee_id']));
		$error = '';
		if(empty($fee_name)) {
	        $error = true;
	        $this->msg['fee_name'] = "Batch is required!";
	    }else if(!preg_match("/^[a-zA-Z\s]+$/",$fee_name)){
	    	$error = true;
	        $this->msg['fee_name'] = "Category Name must only alphabets and space";
	    }

	    if (!$error) {
    		$query = "UPDATE tbl_fee_category SET
    				  fee_name = '$fee_name',
    				  admin_id = '$admin_id'
    				  WHERE fee_id = '$fee_id'";
		    $updatecat = $this->db->update($query);
		    if($updatecat){
		    	$this->msg['success'] = "Updated Successfully!";
		    }else{
		    	$this->msg['error'] = "Error in Updated...Please try again later!!";
		    }
	    }
	     return $this->msg;
    }


    public function delete_student_fee_category($del_cat_id){
		$fee_id = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($del_cat_id));
		$query = "DELETE FROM tbl_fee_category WHERE fee_id = '$fee_id'";
	    $deletecat = $this->db->delete($query);
	    if($deletecat){
	    	$this->msg['success'] = "Deleted Successfully!";
	    }else{
	    	$this->msg['error'] = "Error in Deleted...Please try again later!!";
	    }
	    return $this->msg;
    }

    public function update_status_fee_category($post){

		$fee_status = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['fee_status']));
		$fee_id = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($post['fee_id']));
		$query = "UPDATE tbl_fee_category SET
				  fee_status = '$fee_status'
				  WHERE fee_id = '$fee_id'";
	    $updatecat = $this->db->update($query);
	    if($updatecat){
	    	$this->msg['success'] = "Updated Successfully!";
	    }else{
	    	$this->msg['error'] = "Error in Updated...Please try again later!!";
	    }
     	return $this->msg;
    }

    public function get_money_receipt_fee_category(){
		$query = "SELECT *FROM tbl_fee_category WHERE fee_status = '1'";
	    return $result = $this->db->select($query);
    }

    public function get_student_batch($program){
    	$query = "SELECT DISTINCT student_info.batch_id, batch.batch_name FROM student_info INNER JOIN batch ON student_info.batch_id = batch.batch_id WHERE student_info.department = '$program'";
    	$result = $this->db->select($query);
    	if($result){
    		echo '<option value="">Select Batch</option>';
    		while($row = $result->fetch_assoc()){

    			echo '<option value="'.$row['batch_id'].'">'.$row['batch_name'].'</option>';
    		}
    	}else{
    		echo '<option value="">Batch not available</option>';
    	}

    }

    public function get_student_registrationId($batchid){
	$query = "SELECT *FROM student_info WHERE batch_id = '$batchid'";
    	$result = $this->db->select($query);
    	if($result){
    		echo '<option value="">Select registrationID</option>';
    		while($row = $result->fetch_assoc()){
    			echo '<option value="'.$row['registration_id'].'">'.$row['registration_id'].'</option>';
    		}
    	}else{
    		echo '<option value="">registrationID not available</option>';
    	}
    }

    public function get_student_name($regID){
		$query = "SELECT *FROM student_info WHERE registration_id = '$regID'";
    	$result = $this->db->select($query);
    	if($result){
    		while($row = $result->fetch_assoc()){
    			echo '<span class="dispaly_name ajax_disply_name">'.$row['student_name'].'</span> ';
    		}
    	}else{
    		echo 'Student Name not available';
    	}
    }

    public function save_student_money_receipt($post, $admin_id){
    	$reg_id = $post['registration_id'];
    	$fee_amount = $post['fee_amount'];
    	$sub_total = 0;
		foreach ($fee_amount as $key => $value) {
    		if(!empty($value)){
    			$sub_total +=$value;
    		}
    	}
    	$receipt_date = $this->fm->sql_date_format($post['receipt_date']);
 		$sql = "INSERT INTO st_money_receipt (admin_id, reg_id, sub_total, receipt_date) 
    				  VALUES('$admin_id','$reg_id', '$sub_total', '$receipt_date')";
		$this->db->insert($sql);
		$money_receipt_id = $this->db->link->insert_id;

    	foreach ($fee_amount as $key => $value) {
    		if(!empty($value)){
				$query = "INSERT INTO st_receipt_details (money_receipt_id, fee_id, fee_amount, receipt_date) 
    				  VALUES('$money_receipt_id', '$key', '$value', '$receipt_date')";
			    $insercat = $this->db->insert($query);
			    
    		}

    	}
		if($insercat){
		     $this->msg['success'] = "Save Successfully!";
		    }else{
		     $this->msg['error'] = "Error in Save...Please try again later!!";
		    }
		   return $this->msg;
    }
}