<?php
$path = realpath(dirname(__FILE__));
include_once($path.'/../lib/Controller.php');
/*
* Administation login class
*/

class Administration_login extends Controller{

	public function __construct(){
		parent::__construct();
		$this->db = new Database();
		$this->fm = new Format();
	}

	public function check_authentication($_post){
		$email    = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($_post['email']));
		$password = mysqli_real_escape_string($this->db->link, $this->fm->sanitize($_post['password']));
		if ($this->is_valid_email($email) && $this->is_empty_password($password))
	    {
	        if ($this->login_authentication($email, $password) !=true) {
	             $this->msg['error'] = "Enter Wrong Email or Password...Please try again later!!";
	             return $this->msg;
	        }
	    }

	}
	// function for login authentication or not
	private function login_authentication($email, $password) 
	{
		$salt = sha1(md5($password));
	    $password = md5($password.$salt);

		 $query = "SELECT * FROM admin_info WHERE email = '$email' AND password = '$password'";
	     $result = $this->db->select($query);
	      if($result != false){ // Success
	          $value = $result->fetch_assoc();
	          Session::set('login_status', true);
	          Session::set('fullname', $value['fullname']);
	          Session::set('userID', $value['id']);
	          header("Location:hr/index.php");
	      }else{
	          return false; // Error somewhere
	      }
	}

	private function is_valid_email($email)
	{

	     if (empty($email)) {
	         $this->msg['email']="Email is required.";
	         return $this->msg;
	     } else {
	         // check if e-mail address is well-formed
	         if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		           $this->msg['email']="Invalid email format."; 
		           return $this->msg;
	     		} 
	     // now returns the true- means you can proceed with this mail
	     return true;
		}
	}
	private function is_empty_password($pass) 
	{
	     // Your validation code.
	     if (empty($pass)) {
	         $this->msg['password'] = "Password is required.";
	         return $this->msg;
	     } 
	     // password have
	     return true;
	}
	// function for customer logout
	public function logout_authentication(){
		Session::destroy();

	}
}