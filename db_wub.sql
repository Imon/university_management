-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2017 at 08:41 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_wub`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_info`
--

CREATE TABLE `admin_info` (
  `id` int(11) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mobile_no` varchar(11) NOT NULL,
  `role` varchar(255) NOT NULL DEFAULT 'Administrator',
  `admin_status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;

--
-- Dumping data for table `admin_info`
--

INSERT INTO `admin_info` (`id`, `fullname`, `user_name`, `email`, `password`, `mobile_no`, `role`, `admin_status`) VALUES
(1, 'Azizul Haque', 'azizulhaque', 'azizul@gmail.com', 'f31a0e782084ec906f921920e7f9d6ec', '01833464567', 'Administrator', 1);

-- --------------------------------------------------------

--
-- Table structure for table `batch`
--

CREATE TABLE `batch` (
  `batch_id` int(11) NOT NULL,
  `batch_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `states` varchar(120) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `batch`
--

INSERT INTO `batch` (`batch_id`, `batch_name`, `states`) VALUES
(1, '30', ''),
(2, '30(A)', ''),
(3, '30(B)', ''),
(4, '31', ''),
(5, '31(A)', ''),
(6, '31(B)', '');

-- --------------------------------------------------------

--
-- Table structure for table `student_info`
--

CREATE TABLE `student_info` (
  `id` int(11) NOT NULL,
  `registration_id` varchar(80) NOT NULL,
  `roll_no` varchar(255) NOT NULL,
  `student_name` varchar(80) NOT NULL,
  `father_name` varchar(80) NOT NULL,
  `mother_name` varchar(80) NOT NULL,
  `birth_date` date NOT NULL,
  `mobile_no` varchar(11) NOT NULL,
  `email` varchar(80) NOT NULL,
  `gender` varchar(80) NOT NULL,
  `permanent_address` text NOT NULL,
  `present_address` text NOT NULL,
  `religion` varchar(50) NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `ssc_roll` varchar(80) NOT NULL,
  `ssc_result` varchar(80) NOT NULL,
  `ssc_passing_year` varchar(80) NOT NULL,
  `ssc_board` varchar(80) NOT NULL,
  `ssc_group` varchar(80) NOT NULL,
  `hsc_roll` varchar(80) NOT NULL,
  `hsc_result` varchar(80) NOT NULL,
  `hsc_passing_year` varchar(80) NOT NULL,
  `hsc_board` varchar(80) NOT NULL,
  `hsc_group` varchar(80) NOT NULL,
  `department` varchar(80) NOT NULL,
  `session` varchar(80) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `user_admin` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_info`
--

INSERT INTO `student_info` (`id`, `registration_id`, `roll_no`, `student_name`, `father_name`, `mother_name`, `birth_date`, `mobile_no`, `email`, `gender`, `permanent_address`, `present_address`, `religion`, `blood_group`, `ssc_roll`, `ssc_result`, `ssc_passing_year`, `ssc_board`, `ssc_group`, `hsc_roll`, `hsc_result`, `hsc_passing_year`, `hsc_board`, `hsc_group`, `department`, `session`, `batch_id`, `image`, `user_admin`, `date`) VALUES
(1, 'WUB 03/14/31/1073', '1073', 'Azizul Haque', 'Rafiqul Islam', 'Jahanara', '0000-00-00', '01737416967', 'azizulsw@gmail.com', 'Male', 'Dhaka', 'Dhaka', 'Muslim', 'AB+', '2222', '3.50', '2013', 'BTEB', 'Science', '3333', '3.68', '2015', 'BTEB', 'Science', 'BSc in CSE', '2016-2017', 1, '', 0, '2017-05-02 07:05:16'),
(4, 'WUB 03/14/31/1072', '1072', 'Imon Sarkar', 'Karim', 'Rahima Begum', '1990-02-01', '01745598455', 'hasnain.cse06@yahoo.com', 'Male', 'Dhaka', 'Dhaka', 'Muslim', 'O+', '2222', '3.50', '2015', 'BTEB', 'Science', '3333', '3.68', '2017', 'BTEB', 'Science', 'BSc in CSE', '2016-2017', 1, '../../uploads/f0b6a91368fac9075bd0.jpg', 1, '2017-05-04 09:09:55'),
(5, 'WUB 03/14/31/1010', '1010', 'Arif', 'Kamal', 'Sima', '1994-05-01', '01711510232', 'arif@gmail.com', 'Male', 'Dhaka', 'Dhaka', 'Muslim', 'A+', '101212', '4.00', '2012', 'Dhaka', 'Science', '111111', '3.50', '2014', 'Dhaka', 'Science', 'BSc in EEE', '2016-2017', 4, '', 2, '2017-05-13 19:14:15');

-- --------------------------------------------------------

--
-- Table structure for table `st_money_receipt`
--

CREATE TABLE `st_money_receipt` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `reg_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_total` float(10,2) NOT NULL,
  `receipt_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `st_money_receipt`
--

INSERT INTO `st_money_receipt` (`id`, `admin_id`, `reg_id`, `sub_total`, `receipt_date`) VALUES
(4, 1, 'WUB 03/14/31/1072', 5000.00, '2017-05-14'),
(5, 1, 'WUB 03/14/31/1073', 10000.00, '2017-05-14'),
(6, 1, 'WUB 03/14/31/1073', 10000.00, '2017-05-14');

-- --------------------------------------------------------

--
-- Table structure for table `st_receipt_details`
--

CREATE TABLE `st_receipt_details` (
  `id` int(11) NOT NULL,
  `money_receipt_id` int(11) NOT NULL,
  `fee_id` int(11) NOT NULL,
  `fee_amount` float(10,2) NOT NULL,
  `receipt_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `st_receipt_details`
--

INSERT INTO `st_receipt_details` (`id`, `money_receipt_id`, `fee_id`, `fee_amount`, `receipt_date`) VALUES
(4, 4, 1, 3000.00, '2017-05-14'),
(5, 4, 6, 2000.00, '2017-05-14'),
(6, 5, 1, 10000.00, '2017-05-14'),
(7, 6, 1, 10000.00, '2017-05-14');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fee_category`
--

CREATE TABLE `tbl_fee_category` (
  `fee_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `fee_name` varchar(255) NOT NULL,
  `fee_status` tinyint(1) NOT NULL DEFAULT '1',
  `fee_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;

--
-- Dumping data for table `tbl_fee_category`
--

INSERT INTO `tbl_fee_category` (`fee_id`, `admin_id`, `fee_name`, `fee_status`, `fee_date`) VALUES
(1, 1, 'Monthly Tuition Fee', 1, '2017-05-04 10:09:04'),
(3, 1, 'Semester Wise Tuition Fee', 1, '2017-05-04 10:16:10'),
(6, 1, 'Previous Dues', 1, '2017-05-04 11:13:36');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_info`
--

CREATE TABLE `teacher_info` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `department` varchar(80) NOT NULL,
  `designation` varchar(80) NOT NULL,
  `father_name` varchar(80) NOT NULL,
  `mother_name` varchar(80) NOT NULL,
  `latest_education` varchar(80) NOT NULL,
  `publication_1` varchar(255) NOT NULL,
  `publication_2` varchar(255) NOT NULL,
  `publication_3` varchar(255) NOT NULL,
  `publication_4` varchar(255) NOT NULL,
  `publication_5` varchar(255) NOT NULL,
  `publication_6` varchar(255) NOT NULL,
  `publication_7` varchar(255) NOT NULL,
  `publication_8` varchar(255) NOT NULL,
  `publication_9` varchar(255) NOT NULL,
  `publication_10` varchar(255) NOT NULL,
  `permanent_address` text NOT NULL,
  `present_address` text NOT NULL,
  `birth_date` varchar(80) NOT NULL,
  `joining_date` datetime NOT NULL,
  `mobile_no` varchar(11) NOT NULL,
  `telephone_no` varchar(11) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `religion` varchar(80) NOT NULL,
  `gender` varchar(80) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_info`
--
ALTER TABLE `admin_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batch`
--
ALTER TABLE `batch`
  ADD PRIMARY KEY (`batch_id`);

--
-- Indexes for table `student_info`
--
ALTER TABLE `student_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `st_money_receipt`
--
ALTER TABLE `st_money_receipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `st_receipt_details`
--
ALTER TABLE `st_receipt_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_fee_category`
--
ALTER TABLE `tbl_fee_category`
  ADD PRIMARY KEY (`fee_id`);

--
-- Indexes for table `teacher_info`
--
ALTER TABLE `teacher_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_info`
--
ALTER TABLE `admin_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `batch`
--
ALTER TABLE `batch`
  MODIFY `batch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `student_info`
--
ALTER TABLE `student_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `st_money_receipt`
--
ALTER TABLE `st_money_receipt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `st_receipt_details`
--
ALTER TABLE `st_receipt_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_fee_category`
--
ALTER TABLE `tbl_fee_category`
  MODIFY `fee_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `teacher_info`
--
ALTER TABLE `teacher_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
