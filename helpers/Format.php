<?php
/*
	Format class
*/
class Format{

	public function sql_date_format($date){

		return date('Y-m-d', strtotime($date));
	}

	public function validation( $data ){
	 	$data = trim( $data );
	 	$data = stripcslashes( $data );
	 	$data = htmlspecialchars( $data );
	 	return $data;
	}
	// Sanitize Input data
	public function cleanInput($input) {

	  $search = array(
	    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
	    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
	    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
	    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
	  );

	    $output = preg_replace($search, '', $input);
	    return $output;
	}    
	public function sanitize($input) {
        if (is_array($input)) {
            foreach ($input as $var => $val) {
                $output[$var] = sanitize($val);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $input = stripslashes($input);
            }
            $output = $this->cleanInput($input);
        }
        return $output;
	}


	public function textShorten( $text, $limit = 60 ){
		$text = $text." ";
		$text = substr( $text , 0, $limit);
		$text = substr( $text , 0, strrpos( $text, ' '));
		return $text;
	}
	// ------------- THUMBNAIL (CROP) FUNCTION -------------
	// Function for creating a true thumbnail cropping from any jpg, gif, or png image files
	public function ak_img_thumb($target, $newcopy, $w, $h, $ext) {
	    list($w_orig, $h_orig) = getimagesize($target);
	    $src_x = ($w_orig / 2) - ($w / 2);
	    $src_y = ($h_orig / 2) - ($h / 2);
	    $ext = strtolower($ext);
	    $img = "";
	    if ($ext == "gif"){ 
	    $img = imagecreatefromgif($target);
	    } else if($ext =="png"){ 
	    $img = imagecreatefrompng($target);
	    } else { 
	    $img = imagecreatefromjpeg($target);
	    }
	    $tci = imagecreatetruecolor($w, $h);
	    imagecopyresampled($tci, $img, 0, 0, $src_x, $src_y, $w, $h, $w, $h);
	    if ($ext == "gif"){ 
	        imagegif($tci, $newcopy);
	    } else if($ext =="png"){ 
	        imagepng($tci, $newcopy);
	    } else { 
	        imagejpeg($tci, $newcopy, 84);
	    }
	}

	// -------------- Show thumb image -------------
	public function imageThumb($img){
		$img_ex    = explode('/', $img);
		$img_end   = end($img_ex);
		$img_thumb = "upload/thumb/thumb_".$img_end; 
		return $img_thumb;
	}

	public function url_slug($string, $force_lowercase = true, $anal = false) {
	    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
	                   "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
	                   "â€”", "â€“", ",", "<", ".", ">", "/", "?");
	    $clean = trim(str_replace($strip, "", strip_tags($string)));
	    $clean = preg_replace('/\s+/', "-", $clean);
	    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
	    return ($force_lowercase) ?
	        (function_exists('mb_strtolower')) ?
	            mb_strtolower($clean, 'UTF-8') :
	            strtolower($clean) :
	        $clean;
	}
}
?>